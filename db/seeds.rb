# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Player.destroy_all
Team.destroy_all
Division.destroy_all

['A', 'B'].each do |div|
    division = Division.new name: div
    for t in 1..8 
        team = division.teams.build name: t
        for p in 1..21 
            player = team.players.build rating: rand(0.15..1)
            player.save
        end
        team.rating = team.players.average(:rating)
        team.save
    end
    division.save
end

