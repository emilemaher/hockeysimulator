require 'test_helper'

class DivisionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "playGame" do
    teamA = Team.create name: 'A', rating: 0.25
    teamB = Team.create name: 'B', rating: 0.50
    teamC = Team.create name: 'C', rating: 0.75

    result = Division.playGame(teamA, teamB)
    assert_equal teamB, result[:winner]
    assert_equal teamA, result[:loser]
    result = Division.playGame(teamA, teamC)
    assert_equal teamC, result[:winner]
    assert_equal teamA, result[:loser]
    result = Division.playGame(teamB, teamC)
    assert_equal teamC, result[:winner]
    assert_equal teamB, result[:loser]  
  end

  test "playerRating" do
    assert_empty(Player.where('rating < 0.15 or rating > 1'))
  end

  test "teamRating" do
    assert_empty(Team.where('rating < 0.15 or rating > 1'))
  end

  test "playALot" do
    teamA = Team.create name: 'A', rating: 0.15
    teamB = Team.create name: 'B', rating: 0.50
    results = []
    for i in 1..1000 do
      results << Division.playGame(teamA, teamB)
    end
    assert_equal 1, results
  end

  test "Data counts" do
    assert_equal 2, Division.all.count
    assert_equal 2 * 8, Team.all.count
    assert_equal 2 * 8 * 21, Player.all.count    
  end



end
