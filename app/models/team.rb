class Team < ApplicationRecord
  belongs_to :division
  has_many :players, dependent: :delete_all
end
