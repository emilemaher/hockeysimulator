class Division < ApplicationRecord
    has_many :teams, dependent: :delete_all

    def self.playGame(teamA, teamB)
        scoreA = rand(100)
        scoreB = rand(100)

        if teamA.rating > teamB.rating
            scoreA *= 1 + teamA.rating - teamB.rating
        else
            scoreB *= 1 + teamB.rating - teamA.rating
        end

        winner = scoreA > scoreB ? teamA : teamB
        loser = scoreA > scoreB ? teamB : teamA
        return {
            winner: winner,
            loser: loser,
            score: [scoreA.to_s, scoreB.to_s].join(' / ')
        }
    end

    def self.playTournament(teams = nil)
        winners = []
        teams = self.teams if teams.nil?
        (0..teams.count-1).step(2) do |i|
            result = Division.playGame(teams[i], teams[i+1])
            winners << result[:winner]
        end
        return winners.first if winners.count == 1
        playTournament(winners)
    end

    def self.playAllDivision()
        divA = Division.playTournament Division.first.teams
        divB = Division.playTournament Division.last.teams
        
        winner = Division.playGame(divA, divB)[:winner]
        puts "The winner is team #{winner.name} from division #{winner.division.name}"
    end
end
