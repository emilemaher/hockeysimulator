json.extract! team, :id, :name, :rating, :division_id, :created_at, :updated_at
json.url team_url(team, format: :json)
